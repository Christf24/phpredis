<?php namespace App\Services;

use App\Post as Post;
use Validator;

class PostRegistrar {

	/**
	 * Get a validator for an incoming post submission.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'title' => 'required|max:255',
			'author' => 'required|max:255',
			'tags'	 => 'max:50',
			'content' => 'required|min:6',
		]);
	}

	/**
	 * Create a new post instance after a valid submission.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return Post::create([
			'title' => $data['title'],
			'author' => $data['author'],
			'content' => $data['content'],
		]);
	}

	/**
	 * Add tags for post
	 *
	 * @param  array  $data
	 * @return int
	 */
	// public function addTags( array $data )
	// {
	// 	return Post::addTags([
	// 		'postID' => $data['postID'],
	// 		'tags' => $data['tags'],
	// 	]);
	// }

}
