<?php namespace App\Http\Controllers;

use Request;
use Redis;
use App\Contracts\UpdateContract as UpdateContract;
use App\Services\UpdateRegistrar as UpdateRegistrar;

class UserController extends Controller {

	protected $update;

	protected $updateRegistrar;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UpdateContract $update, UpdateRegistrar $updateRegistrar)
	{
		$this->update = $update;
		$this->updateRegistrar = $updateRegistrar;
	}

	/**
	 * Give admin ability to add a post
	 *
	 * @return Response
	 */
	public function showAddUpdate($id)
	{
		return view('user.update')->with([ 'userID' => $id ]);
	}

	/**
	 * Create the post from user inputs
	 * 
	 * @return Response
	 */
	public function doAddUpdate( $id )
	{
		$userInput = array(
			'update' 	=> Request::input('update')
		);

		$validation = $this->updateRegistrar->validator($userInput);

		if ( $validation )
		{
			// Create update and push to followers
			$result = $this->updateRegistrar->create($userInput, $id);

			if ( $result )
			{
				// Update posted. Redirect to newsfeed
				return view('user.newsfeed')->with('message', 'Update successfully posted');	
			}

		}

		// Update did not post successfully
		return view('update')->withMessage('Update did not post. Please try again');
	}

	/**
	 * Show list of all users except current user
	 * 
	 * @param  $id
	 * @return Response
	 */
	public function showUserList( $id )
	{
		$userList = Redis::lRange('users', 0, 6);

		if ( $userList != '' )
		{

			$users = [];

			foreach ($userList as $value) 
			{
				// We need just the ID number for the follow URL
				$filteredID = str_replace('user:', '', $value);

				// Don't show the current user
				if ( $filteredID != $id )
				{
					$users[$value]['username'] = ucfirst(Redis::hGet($value, 'username'));
					$users[$value]['id'] = $filteredID;
				}

			}
			return view('user.userlist')->with([ 'users' => $users, 'current_user_id' => $id ]);
		}

		return view('user.userList')->withMessage('No users to follow');
	}

	/**
	 * Follow a user
	 * 
	 * @param  $id     ID of current user
	 * @param  $userID ID of user to follow
	 * @return Response
	 */
	public function followUser( $id, $userID )
	{
		// Add follower ID to user being followed at the current time
		Redis::zAdd( 'followers:' . $userID, time(), $id );

		// Add userID of person the current user decided to follow
		Redis::zAdd( 'following:' . $id, time(), $userID );

		return view('user.followsuccess')->with([ 'message' => 'Successfully followed user.', 'user_id' => $id ]);
	} 

	/**
	 * Show user news feed
	 * 
	 * @param  $userID
	 * @return Response
	 */
	public function showFeed($userID)
	{
		// Get 40 posts from people user follows
		Redis::ltrim('posts:' . $userID, 0, 100);

		// Get post IDs
		$postIDs = Redis::lRange('posts:' . $userID, 0, 100);

		// Get post information from IDs
		$posts = $this->update->getAllPosts($postIDs);

		return view('user.newsfeed')->withPosts($posts);
	}
}
