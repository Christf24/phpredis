<?php namespace App;

use Redis;
use App\Contracts\UpdateContract as UpdateContract;
use Illuminate\Database\Eloquent\Model;

class Update implements UpdateContract {

	public static function create( array $data )
	{
		// Create unique post ID
		$postID = Redis::incr('next_post_id');

		$time = time();

		// Create the post message
		$postSuccess = Redis::hmset('post:' . $postID, [ 'user_id' => $data['userID'], 'time' => $time, 'text' => $data['update'] ]);

		if ($postSuccess)
		{
			// Push update to followers
			if ( Update::pushUpdate($postID, $data['userID']) )
			{
				return $postID;
			}

			// Unsuccessful push to followers. Delete post and decrement ID
			// In real world case we would queue and try again or give user another chance
			Redis::delete('post:' . $postID);
			Redis::decr('next_post_id');
		}

		return false;		
	}

	public static function pushUpdate($postID, $userID)
	{
		// Get all the user's followers
		$followers = Redis::zRange('followers:' . $userID, 0, -1);

		// Include author
		$followers[] = $userID;

		// Push the update to all followers
		foreach ($followers as $followerID) 
		{
			$pushSuccess = Redis::lpush('posts:' . $followerID, $postID);
		}

		// Success of update push to followers
		if ($pushFollowers)
		{
			return true;
		}

		return false;
	}

	public function getAllPosts($postIDs)
	{
		$posts = [];

		foreach($postIDs as $post)
		{
			// $posts[$post] = Redis::hMGet('post:' . $post, [ 'user_id', 'time', 'text' ]);
			$posts[$post] = Redis::hGetAll('post:' . $post);

			$userName = Redis::hGet('user:' . $posts[$post]['user_id'], 'username');

			$posts[$post]['username'] = $userName;
		}

		return $posts;
	}

}
